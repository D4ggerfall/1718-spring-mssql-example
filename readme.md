#Spring Backend for Untis

##What this project is about

This project uses the data of the Untis timetable of the HTL Spengergasse. It uses Spring Boot and gives out json objects.


This project follows the model - repository - service - controller scheme of Spring.


##What you need to run this project

You need an up and running database, which you can create via the provided script. The way of inserting the data may vary for each SQL dialect.
This should preferably be done in a local SQL Server, since the script and properties are made for SQL Server 2016.
If you do decide to use another database provider, you will have to change the properties and the script.